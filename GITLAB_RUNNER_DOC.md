Installing the Gitlab Runner
============================
In GitLab CI/CD, Runners run the code defined in .gitlab-ci.yml. A GitLab Runner is a lightweight, highly-scalable agent that picks up a CI job through the coordinator API of GitLab CI/CD, runs the job, and sends the result back to the GitLab instance.

Create a new group
------------------

- https://docs.gitlab.com/ee/user/group/#create-a-new-group

Add users to a group
--------------------

- https://docs.gitlab.com/ee/user/group/#add-users-to-a-group

Add projects to a group
------------------------

- https://docs.gitlab.com/ee/user/group/#add-projects-to-a-group

Pipeline schedules
--------------------
- https://docs.gitlab.com/ee/ci/pipelines/schedules.html

- https://docs.gitlab.com/ee/ci/pipelines/#run-a-pipeline-manually

To install the Runner:
---------------------
-  Add GitLab’s official repository: 

       $ curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
-  Install the latest version of GitLab Runner, or skip to the next step to install a specific version: 

       $ sudo apt-get install gitlab-runner
-  To install a specific version of GitLab Runner: 

       $ apt-cache madison gitlab-runner

       $ sudo apt-get install gitlab-runner=10.0.0

Change Gitlab CI Runner user
---------------------------

-  Create or edit a /etc/systemd/system/gitlab-runner.service file, with content::

       [Unit]

       Description=GitLab Runner

       After=syslog.target network.target

       ConditionFileIsExecutable=/usr/bin/gitlab-runner
 
       [Service]

       StartLimitInterval=5

       StartLimitBurst=10

       ExecStart=/usr/bin/gitlab-runner "run" "--working-directory" "/home/user" "--
       config" "/etc/gitlab-runner/config.toml" "--service" "gitlab-runner" "--syslog" "--user"   "user"Restart=always
  
       RestartSec=120
  
       [Install]
  
       WantedBy=multi-user.target

-  change the --user and --working dir path accordingly
```sh
 # Alternate Method
 $ gitlab-runner uninstall

 $ gitlab-runner install --working-directory /home/ubuntu --user ubuntu
```


-  Execute $ systemctl daemon-reload

  
Registering Runners
--------------------

   Registering a Runner is the process that binds the Runner with a GitLab instance.
   
 - Requirements

   Before registering a Runner, you need to first:

    - Install it on a server separate than where GitLab is installed
    - Obtain a token:
        - For a shared Runner, have an administrator go to the GitLab Admin Area and click Overview > Runners
        - For a group Runner, go to Settings > CI/CD and expand the Runners section
        - For a project-specific Runner, go to Settings > CI/CD and expand the Runners section 


 - To register a Runner under GNU/Linux:

    Run the following command:

       $ sudo gitlab-runner register --locked="false"

 - Enter your GitLab instance URL:
  Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com )

       $ https://gitlab.com

 - Enter the token you obtained to register the Runner:
  Please enter the gitlab-ci token for this runner:
   # (For a group Runner, go to Settings > CI/CD expand the Runners section,
       - Set up a group Runner manually
        Install GitLab Runner
       - Specify the following URL during the Runner setup: 
        https://gitlab.com/ 

       - Use the following registration token during setup: 
        XXXXXXX
       - Start the Runner! )

 - Enter a description for the Runner, you can change this later in GitLab’s UI:
  Please enter the gitlab-ci description for this runner
       $ [hostname] my-runner

 - Enter the tags associated with the Runner, you can change this later in GitLab’s UI:
  Please enter the gitlab-ci tags for this runner (comma separated):
       $ my-tag

 - Enter the Runner executor:
  Please enter the executor: ssh, docker+machine, docker-ssh+machine, kubernetes, docker, parallels, virtualbox, docker-ssh, shell:
       $ shell

- Runner path

       $ mkdir /scratch/runner-dir
       $ chmod 777 -R scratch/runner-dir

- Edit /etc/gitlab-runner/config.toml file 

       $ sudo vi /etc/gitlab-runner/config.toml


       concurrent = 2

       check_interval = 0

       [[runners]]
       name = "xxxxxx(gitlab-ci description)"

       url = "https://gitlab.com/"

       token = "XXXXXXXXXXXXXXXX"

       executor = "shell"

       builds_dir = "/scratch/runner-dir"

       [runners.cache]


- Restart the service

       $ sudo gitlab-runner restart


You can check a recent list of commands by executing:

- gitlab-runner --help

Append --help after a command to see its specific help page:

- gitlab-runner <command> --help

*Links:*
--------

https://docs.gitlab.com/runner/install/linux-repository.html

https://docs.gitlab.com/runner/configuration/advanced-configuration.html

https://docs.gitlab.com/runner/register/
 
https://docs.gitlab.com/runner/executors/README.html

https://docs.gitlab.com/runner/commands/


